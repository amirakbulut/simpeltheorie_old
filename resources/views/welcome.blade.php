@extends('layouts.index')

@section('content')

<!-- MAIN -->
<main class="home">
    <div class="container">
        <section class="text-block mb-70">
        </br>
            <h4 class="text-center">Gegarandeerd slagen met online examen trainingen</h4>
            <p class="text-center">We are a small team of 3 people making themes and doing custom design work. If you can’t find what you looking for in our store section feel free to contact us with a custom job request.</p>
        </section>
        <a href=".?page=signup" class="btn btn-large btn-center mb-60">Direct starten voor mijn theorie <i class="arrow-right"></i></a></br>

        <section class="offers anim-scale row">
            <div class="col-md-3 col-sm-6 col-xs-6">
                <a href="#">
                    <div class="hover"></div>
                    <img src="{{ asset('images/index/svg/PSD.svg') }}" alt="">
                    <div class="carddiv">
                        <p class="title">Leerpakketten</p>
                        <p>Examenstof gesorteerd op onderdelen en prioriteit.</p>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <a href="#">
                    <div class="hover"></div>
                    <img src="{{ asset('images/index/svg/wordpress.svg') }}" alt="">
                    <div class="carddiv">
                        <p class="title">Digitaal leren</p>
                        <p>Weg met ouderwetse boeken en cd's, maar makkelijk online leren.</p>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <a href="#">
                    <div class="hover"></div>
                    <img src="{{ asset('images/index/svg/app.svg') }}" alt="">
                    <div class="carddiv">
                        <p class="title">Overal</p>
                        <p>In de bus of de trein? Leren kan overal, simpelweg met je mobiel.</p>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <a href="#">
                    <div class="hover"></div>
                    <img src="{{ asset('images/index/svg/statistics.svg') }}" alt="">
                    <div class="carddiv">
                        <p class="title">Statistieken</p>
                        <p>Eenvoudig bijhouden welke onderdelen het meest je aandacht nodig hebben.</p>
                    </div>
                </a>
            </div>
        </section>

        <section class="row mb-40">
            <div class="col-md-4">
                <a href="#" class="comment">
                    <div class="image">
                        <img src="{{ asset('images/index/1.png') }}" alt="">
                    </div>
                    <p>The customer support was incredible.</br>It was very prompt and helpful replies.</p>
                    <div class="caption">
                        <p>Stan Marsh</p>
                        <p>CEO, horizon.com</p>
                    </div>
                </a>
            </div>
            <div class="col-md-8">
                <a href="$" class="info">
                    <img src="{{ asset('images/index/2.png') }}" alt="">
                    <div class="text">
                        <h4 class="border-decor">We transform great ideas into amazing digital products for your business.</h4>
                        <p>We hope this guide has been helpful. It's all about balance and being aware of the different types of visitors to your site. </p>
                    </div>
                </a>
            </div>
        </section>
        <section class="row mb-40">
            <div class="col-md-8">
                <div class="block-icon">
                    <img src="{{ asset('images/index/svg/support.svg') }}" alt="">
                    <div class="text-block">
                        <h4 class="text-center border-decor">We offer fast & top-notch customer support.</h4>
                        <p class="text-center">Our solution experts are here to answer your questions, help you troubleshoot issues, improve your experience & increase satisfaction with our solutions.</p>
                    </div>
                    <a href="#" class="btn btn-small btn-center">Support Centre</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="download">
                    <div class="text-block">
                        <h4 class="border-decor">Grab this month’s free item.</h4>
                        <p>Download this month’s free item. Get it while you can!</p>
                    </div>
                    <img src="{{ asset('images/index/3.png') }}" alt="">
                    <a href="#" class="btn btn-small btn-dark">Download</a>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection
