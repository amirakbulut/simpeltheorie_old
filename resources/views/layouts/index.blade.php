<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Simpel Theorie">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Simpel Theorie') }}</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/index/index.css') }}" rel="stylesheet">


        <link rel="shortcut icon" type="image/x-icon" href="./images/favicon.ico"/>


    </head>
    <body>

        <!-- HEADER -->
        <header>
            <div class="container">
                <a href="./?page=home" class="logo pull-left">
                    <img src="{{ asset('images/index/svg/logo.svg') }}" alt="logo">
                </a>
                <nav>
                    <div class="dropdown pull-left">
                        <a data-toggle="dropdown" href="#">Store <b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li><a href="index.html">Home Page</a></li>
                            <li><a href="browse.html">Browse Page</a></li>
                        </ul>
                    </div>
                    <ul class="list-inline pull-left">
                        <li><a href="services.html">Services</a></li>
                        <li><a href="contact_support.html">Support</a></li>
                        <li><a href="works.html">Custom Job</a></li>
                        <li><a href="pricing.html">Pricing</a></li>
                    </ul>
                    <ul class="list-inline pull-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">{{ __('Login') }}</a></li>
                            <li><a href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @else
                            <li><a href="/home">Dashboard</a></li>
                        @endguest
                    </ul>
                </nav>
            </div>
        </header>

    <!-- content -->
    @yield('content')

    <!-- FOOTER -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <a href="index.html" class="logo">
                        <img src="{{ asset('images/index/svg/logo.svg') }}" alt="logo">
                    </a>
                    <p>From logo design to web development expert designers, developers are ready to complete your projects.</p>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <ul class="footer-menu">
                        <li>
                            <a href="#" class="border-decor">Store</a>
                            <ul>
                                <li><a href="works.html">Wordpress</a></li>
                                <li><a href="works.html">PSD Theme</a></li>
                                <li><a href="works.html">Icons Packs</a></li>
                                <li><a href="works.html">Site Template</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="border-decor">Info</a>
                            <ul>
                                <li><a href="services.html">Services</a></li>
                                <li><a href="contact_support.html">Contact</a></li>
                                <li><a href="pricing.html">Pricing</a></li>
                                <li><a href="index.html">Blog</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="clearfix hidden-md hidden-lg"></div>
                <div class="col-md-4 col-sm-12">
                    <p>Subscribe to our newsletter to receive news, updates, free stuff and new releases by email.</p>
                    <form action="#" method="post">
                        <input type="text" class="form-control" placeholder="Email Address" />
                        <input type="submit" class="btn btn-small btn-gray" value="Subscribe" />
                    </form>
                    <ul class="icons">
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="copyright pull-right">&#169; 2019 Simpel Theorie</div>
                </div>
            </div>
        </div>
    </footer>

    <!-- isotope
    <script src="./includes/home/js/isotope.pkgd.min.js" type="text/javascript"></script>
    <script src="./includes/home/js/isotope.packery.js" type="text/javascript"></script>
     -->

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/index/index.js') }}" defer></script>

    </body>
</html>
