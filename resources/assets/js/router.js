import Vue from 'vue'
import VueRouter from 'vue-router'

import index from './views/index'
import Menu_item from './views/item'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/home',
            name: 'home',
            component: index
        },
        {
            path: '/item',
            name: 'item',
            component: Menu_item
        }
    ],
})

const app = new Vue({
    delimiters: ["<%","%>"],
    router,
}).$mount('#app')
