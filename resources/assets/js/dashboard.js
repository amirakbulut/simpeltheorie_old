require('jquery-slimscroll');
require('jquery.uniform');
require('switchery');
require('d3');
require('nvd3');
require('jquery.flot');
require('chart.js');
require('./router.js');


$(function () {
  $('[data-toggle="tooltip"]').tooltip();


  $('.progress-bar').each(function() {
      if($(this).data('value') >= 75){
          $(this).css('background-color', '#8bc34a');
          $(this).closest('.task-item').find('.task-name').append('<i data-toggle="tooltip" data-placement="right" title="Voldoende" class="fa fa-check"></i>');

      }
  });

});
